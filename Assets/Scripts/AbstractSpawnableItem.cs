﻿using UnityEngine;
using Assets.Scripts.SpawnPatterns;

public abstract class AbstractSpawnableItem : MonoBehaviour {
    bool GambyMode = true;

    protected SpawnPatterns spt;
    protected GameControllerScript gcs;
    void Awake() {
        gcs = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameControllerScript>();
    }

    public void getSpawnPattern(SpawnPatterns spt) {
        this.spt = spt;
    }

    void OnDestroy()
    {
        spt.PatternIsFinished();
    }

    void OnTriggerExit2D(Collider2D other) {
        if (!GambyMode)
            Destroy(this.gameObject);
        else
            GambyMode = false;
    }

}
