﻿using UnityEngine;
using UnityEngine.Advertisements;

public class AdsScript : MonoBehaviour
{
    [SerializeField] private StartMenuScript startMenu;

    public void ShowRewardedAd() {
        print("ShowRewardedAd");
        if (Advertisement.IsReady("rewardedVideo")) {
            var options = new ShowOptions { resultCallback = HandleShowResult };
            Advertisement.Show("rewardedVideo", options);
        }
    }

    private void HandleShowResult(ShowResult result) {
        switch (result) {
            case ShowResult.Finished:
                Debug.Log("The ad was successfully shown.");
                Finished();
                break;
            case ShowResult.Skipped:
                Debug.Log("The ad was skipped before reaching the end.");
                Failed();
                break;
            case ShowResult.Failed:
                Debug.LogError("The ad failed to be shown.");
                Failed();
                break;
        }
    }
    void Failed()
    {
        //Gcs.DeadAndGone();
    }

    void Finished()
    {
        startMenu.AddLife();
    }
}