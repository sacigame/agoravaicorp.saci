﻿using UnityEngine;
using System.Collections;

public class DamageItem : AbstractSpawnableItem {
    [SerializeField]
    int damage;

    void OnTriggerEnter2D(Collider2D other) {
        if (other.tag == "Player") {
            gcs.GimmeDamage(damage);
            Destroy(this.gameObject);
        }
    }
}
