﻿using UnityEngine;
using System;
using UnityEngine.EventSystems;
using Assets.Scripts.Enum;
using Assets.Scripts.Entities;

public class DragQueen : MonoBehaviour, IDragHandler, IBeginDragHandler, IEndDragHandler {

    float start, end;
    bool toque;
    [SerializeField]
    public float safeDistance = 1;
	[SerializeField]
	Controle FonteControle;

    Vector2 ChangeReferenceToCenter(Vector2 vectorPosition) {
        Ray ray = Camera.main.ScreenPointToRay(vectorPosition);
        float distance = Vector3.Distance(transform.position, Camera.main.transform.position);
        Vector3 rayPoint = ray.GetPoint(distance);
        return new Vector2(rayPoint.x, rayPoint.y);
    }

    #region Events

    public delegate void DragEventHandler(object source, DragArgs args);

    public event DragEventHandler DragDado;

    protected virtual void OnDragDado(InputDirectionsEnum inputDirectionsEnum) {
        if (DragDado != null)
            DragDado(this, new DragArgs() { Direcao = inputDirectionsEnum });
    }

    public void OnBeginDrag(PointerEventData ped) {
        toque = true;
        start = ChangeReferenceToCenter(ped.position).y;
    }

    public void OnDrag(PointerEventData ped) {
        float distance = Math.Abs(start - ChangeReferenceToCenter(ped.position).y);
        if (toque && distance > safeDistance) {
            OnDragDado((ChangeReferenceToCenter(ped.position).y - start) > 0 ? InputDirectionsEnum.Up : InputDirectionsEnum.Down);
            toque = false;
        }
    }

    public void OnEndDrag(PointerEventData ped) {
        toque = false;
        OnDragDado(InputDirectionsEnum.Undefined);
    }

    #endregion

    #region Botões

    public void Direita() {
        OnDragDado(InputDirectionsEnum.Right);
    }

    public void Esquerda() {
        OnDragDado(InputDirectionsEnum.Left);
    }

    public void Stop() {
        OnDragDado(InputDirectionsEnum.Undefined);
    }

    #endregion

	#region Acelerometro

	void Acelerometro(){
		if (Input.acceleration.x > 0.15f)
			OnDragDado (InputDirectionsEnum.Right);
		else if (Input.acceleration.x < -0.15f)
			OnDragDado (InputDirectionsEnum.Left);
		else
			OnDragDado (InputDirectionsEnum.Undefined);
	}

	void Update(){
		if (FonteControle == Controle.Acelerometro) {
			Acelerometro ();
		}
			
		if(Input.GetKeyDown (KeyCode.UpArrow))
			OnDragDado(InputDirectionsEnum.Up);
		else if(Input.GetKeyDown(KeyCode.DownArrow))
			OnDragDado(InputDirectionsEnum.Down);
		else if(Input.GetKeyDown (KeyCode.RightArrow))
			OnDragDado(InputDirectionsEnum.Right);
		else if(Input.GetKeyDown (KeyCode.LeftArrow))
			OnDragDado(InputDirectionsEnum.Left);
		else if(Input.GetKeyUp(KeyCode.LeftArrow) || Input.GetKeyUp(KeyCode.RightArrow))
			OnDragDado(InputDirectionsEnum.Undefined);
	}

	#endregion
}
