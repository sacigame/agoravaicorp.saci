﻿using UnityEngine;

namespace Assets.Scripts.Editable {
    [CreateAssetMenu(fileName = "LaneSet", menuName = "LaneSet")]
    public class LaneSet : ScriptableObject {
        public GameObject ObjectLane;
        public float XPosition;

        public LaneSet(GameObject spawnObject, float xPosition) {
            this.ObjectLane = spawnObject;
            this.XPosition = xPosition;
        }
    }

}

