﻿using Assets.Scripts.Enum;
using System;

namespace Assets.Scripts.Entities {
    public class DragArgs : EventArgs {
        public InputDirectionsEnum Direcao { get; set; }
    }
}
