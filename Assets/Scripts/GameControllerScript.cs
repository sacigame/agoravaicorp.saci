﻿using UnityEngine;
using System;
using System.Collections;
using Assets.Scripts.Enum;
using Assets.Scripts.Entities;
using System.Collections.Generic;


using UnityEngine.SocialPlatforms;

public class GameControllerScript : MonoBehaviour {
    #region Variables
    [SerializeField]
    private AudioSource ButtonSound;
    [SerializeField] private AudioSource Maca;

    [SerializeField]
    private AudioSource Rede;

    [SerializeField]
    SpawnController mSpawnController;

    [SerializeField]
    DragQueen mDragControlButton;

    [SerializeField]
    DragQueen mDragControlAcelerometro;

    [SerializeField]
    LifeDisplayScript mLifeDisplay;

    [SerializeField]
    Saci jogador;

    [SerializeField]
    List<GameObject> andares;

    [SerializeField]
    UnityEngine.UI.Text scoreDisplay;

    public GoogleAnalyticsV3 googleAnalytics;

    public int GameScore { get; private set; }

    private const int PointsPerLevel = 50;
    private const int MaximumLevel = 10;

    public List<GameObject> Andares {
        get {
            return andares;
        }
    }

    public int GameDifficult { get; private set; }

    #endregion

    #region MonobBehaviourFunctions
    void Awake() {
        this.GameDifficult = PointsPerLevel;
        this.scoreDisplay.text = "Score 0";
        inscreve();
        CntrDoJogador = (Controle)PlayerPrefs.GetInt("ControleDoJogador");
        /* controles */
        if (CntrDoJogador == Controle.Botao)
            UsaBotao();
        else if (CntrDoJogador == Controle.Botao)
            UsaAcelerometro();
    }

    void Start() {
        mLifeDisplay.UpdateLifeToDisplay(jogador.Vida);
        print("jogador.vida" + jogador.Vida);



        Social.localUser.Authenticate((bool success) => { });
    }
    void Update() {
        if (Input.GetKeyDown(KeyCode.P))
            GimmePoints(10);
    }
    #endregion

    #region Eventos

    void inscreve() {
        mDragControlButton.DragDado += this.OnDragDado;
        mDragControlAcelerometro.DragDado += this.OnDragDado;
    }

    public void OnDragDado(object obj, DragArgs args) {
        jogador.Move(args.Direcao);
    }

    #endregion  

    #region Score/Damage
    public void InitLifeDisplay(int PlayerLifes) {
        mLifeDisplay.UpdateLifeToDisplay(PlayerLifes);
    }

    public void GimmePoints(int score) {
        Maca.Play();
        GameScore += score;
        this.scoreDisplay.text = string.Format("Score {0}", GameScore);
        CheckDificult();
    }


    public void GimmeDamage(int damage) {
        Rede.Play();
        jogador.Vida -= damage;
        print("vida = " + jogador.Vida);
        CheckLife(jogador.Vida);
        mLifeDisplay.UpdateLifeToDisplay(jogador.Vida);
        Handheld.Vibrate();
    }

    void CheckLife(int vida) {
        if (vida < 1) {
            jogador.Morre();
            StartCoroutine(FimDeJogo(5));
        }
        else {
            jogador.Morre();
            base.StartCoroutine(this.TimeToComeBack(0.5f));
        }
    }

    #endregion

    IEnumerator TimeToComeBack(float secs) {
        yield return new WaitForSeconds(secs);
        this.jogador.Revive();
    }

    IEnumerator FimDeJogo(float secs) {
        yield return new WaitForSeconds(secs);
        DeadAndGone();
    }

    public void DeadAndGone() {
        PlayerPrefs.SetInt("Lifes", 3);
        SendPlayerCtrlToAnalytics();
        Social.ReportScore(this.GameScore, "CggI8PSlrgkQAhAB", (bool success) => { });
        UnityEngine.SceneManagement.SceneManager.LoadScene("StartMenu");
    }

    void CheckDificult() {
        if (this.GameScore == this.GameDifficult && this.GameScore < MaximumLevel * PointsPerLevel) {
            mSpawnController.SpeedUp();
            //this.SpawnInterval = this.SpawnInterval - (this.SpawnInterval / 3);
            this.GameDifficult += PointsPerLevel;
        }
        if ((GameScore % (PointsPerLevel * 5)) == 0)
            LifeUp();
    }

    void LifeUp() {
        if (jogador.Vida < 3) {
            jogador.Vida++;
            mLifeDisplay.UpdateLifeToDisplay(jogador.Vida);
        }
    }

    #region TipoDeControle
    [SerializeField]
    GameObject ControleBotao;
    [SerializeField]
    GameObject ControleAcelerometro;

    public static Controle CntrDoJogador;

    public void UsaBotao() {
        ControleBotao.SetActive(true);
        ControleAcelerometro.SetActive(false);
        CntrDoJogador = Controle.Botao;
        if (Time.timeScale == 0)
            this.BotaoPause();
        PlayerPrefs.SetInt("ControleDoJogador", (int)Controle.Botao);
    }

    public void UsaAcelerometro() {
        ControleBotao.SetActive(false);
        ControleAcelerometro.SetActive(true);
        CntrDoJogador = Controle.Acelerometro;
        if (Time.timeScale == 0)
            this.BotaoPause();
        PlayerPrefs.SetInt("ControleDoJogador", (int)Controle.Acelerometro);
    }

    #endregion

    #region Pause
    [SerializeField]
    Canvas PauseMenu;

    public void BotaoPause() {
        ButtonSound.Play();
        Time.timeScale = (Time.timeScale == 0 ? 1 : 0);
        PauseMenu.enabled = !PauseMenu.enabled;
    }

    public void BotaoMenu() {
        this.BotaoPause();
        UnityEngine.SceneManagement.SceneManager.LoadScene("StartMenu");
    }

    #endregion

    #region Analytics

    void SendPlayerCtrlToAnalytics() {
        googleAnalytics.LogEvent(new EventHitBuilder()
            .SetEventCategory("Controle")
            .SetEventLabel(CntrDoJogador.ToString()));
        print(CntrDoJogador.ToString());
    }

    #endregion
}
