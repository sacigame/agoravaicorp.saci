﻿using UnityEngine;
using System.Collections;

public class LifeDisplayScript : MonoBehaviour
{
    [SerializeField] private UnityEngine.UI.Text LifeDisplay;

    public void UpdateLifeToDisplay(int lifes)
    {
        LifeDisplay.text = lifes.ToString();
    }

}
