﻿using UnityEngine;
using System.Collections.Generic;

public class LoadHowTo : MonoBehaviour {
    [SerializeField]
    List<GameObject> HowToSequence;

    private int ImageIndex;

    void Awake() {
        this.ImageIndex = 0;
        HowToSequence[this.ImageIndex].SetActive(true);
    }

    public void OnClickLoadHowToImage() {
        GetComponent<AudioSource>().Play();
        HowToSequence[this.ImageIndex].SetActive(false);
        this.ImageIndex++;
        if (this.ImageIndex < HowToSequence.Count) {
            HowToSequence[this.ImageIndex].SetActive(true);
        }
        else {
            this.ImageIndex--;
            HowToSequence[this.ImageIndex].SetActive(true);
            UnityEngine.SceneManagement.SceneManager.LoadScene("Jogo");
        }
    }
}