﻿using UnityEngine;
using System.Collections;
using System;
using Assets.Scripts.Enum;

public class InputArgs : EventArgs {

    public InputDirectionsEnum direcao { get; set; }

}

public class OBSOLETE_InputControle : MonoBehaviour {
    float start, end;

    [SerializeField]
    public float safeDistance = 1;

    InputDirectionsEnum directions;

    Vector2 fixedPosition() {
        Ray ray = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);
        float distance = Vector3.Distance(transform.position, Camera.main.transform.position);
        Vector3 rayPoint = ray.GetPoint(distance);
        return new Vector2(rayPoint.x, rayPoint.y);
    }
    public bool Usavel = true;
    float movendo;//-1 se nao 1 se sim

    public delegate void InputEventHandler(object sourse, InputArgs args);

    public event EventHandler<InputArgs> InputDado;

    void Update() {
        //Usavel = false;
        if (Input.touchCount > 0) {
            if (Input.GetTouch(0).phase == TouchPhase.Began) {
                //start = Input.GetTouch(0).position.y;
                start = fixedPosition().y;
                StartCoroutine(DefineDirection(0.3f));
            }
        }
    }


    protected virtual void OnInputDado(InputDirectionsEnum ipd) {
        if (InputDado != null)
            InputDado(this, new InputArgs() { direcao = ipd });
    }

    IEnumerator DefineDirection(float seconds) {
        yield return new WaitForSeconds(seconds);
        float distance = Math.Abs(start - fixedPosition().y);

        if (distance <= this.safeDistance) {
            OnInputDado(fixedPosition().x > 0 ? InputDirectionsEnum.Right : InputDirectionsEnum.Left);
        }
        else {
            OnInputDado(fixedPosition().y > start ? InputDirectionsEnum.Up : InputDirectionsEnum.Down);
        }
    }
}
