﻿using UnityEngine;
using System;

public class PointItem : AbstractSpawnableItem {
    [SerializeField]
    int points;

    void OnTriggerEnter2D(Collider2D other) {
        if (other.tag == "Player") {
            gcs.GimmePoints(points);
            Destroy(this.gameObject);
        }
    }
}
