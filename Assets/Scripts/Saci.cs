﻿using UnityEngine;
using System.Collections;
using Assets.Scripts.Enum;

public class Saci : MonoBehaviour {

    [SerializeField]
    GameControllerScript gcs;

    [SerializeField]
    float veloc;

    int andarAtual;
	public int AnadarAtual{ get; private set; }
    public int Vida;
    public bool Movivel { get; set; }

    bool olhandoDireita;

    void Awake()
    {
        Vida = PlayerPrefs.GetInt("Lifes");
    }

    void Start()
    {
        print("vida" + Vida);
        Movivel = true;
        andarAtual = Mathf.CeilToInt(gcs.Andares.Count / 2);
        MoveParaAndar(andarAtual);
    }

    void MoveParaAndar(int index) {
        this.transform.SetParent(gcs.Andares[index].transform);
        this.transform.localPosition = new Vector2(this.transform.position.x, 0);
    }

    public void Move(InputDirectionsEnum ipd) {
        if (Movivel) {
            switch (ipd) {
                case InputDirectionsEnum.Up:
                    if (andarAtual < gcs.Andares.Count - 1) {
                        andarAtual += 1;
                        MoveParaAndar(andarAtual);
                    }
                    break;
                case InputDirectionsEnum.Down:
                    if (andarAtual > 0) {
                        andarAtual -= 1;
                        MoveParaAndar(andarAtual);
                    }
                    break;
                case InputDirectionsEnum.Right:
                    this.GetComponent<Rigidbody2D>().velocity = new Vector2(veloc, 0);
                    if (!olhandoDireita) {
                        gameObject.transform.Rotate(Vector3.up, 180, Space.World);
                        olhandoDireita = true;
                    }
                    break;
                case InputDirectionsEnum.Left:
                    this.GetComponent<Rigidbody2D>().velocity = new Vector2(-veloc, 0);
                    if (olhandoDireita) {
                        gameObject.transform.Rotate(Vector3.up, 180, Space.World);
                        olhandoDireita = false;
                    }
                    break;
                default:
                    this.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
                    break;
            }
        }
    }

    public void Morre() {
        this.GetComponent<Animator>().SetBool("saciPreso", true);
        Movivel = false;
        this.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        GetComponent<BoxCollider2D>().enabled = false;
    }

    public void Revive() {
        this.GetComponent<Animator>().SetBool("saciPreso", false);
        Movivel = true;
        //this.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        GetComponent<BoxCollider2D>().enabled = true;
    }

}