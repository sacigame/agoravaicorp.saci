﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.SpawnPatterns;

public class SpawnController : MonoBehaviour {
    #region SerializeFields
    [SerializeField]
    GameControllerScript GameController;

    [SerializeField]
    float Veloc;

    [SerializeField]
    public float FixNoX;

    [SerializeField]
    GameObject[] patterns;

	[SerializeField]
	Animation SpeedUpAnim;

    #endregion

    private float SpawnInterval;
    private List<GameObject> _andares;
    private int _score;

    public bool PatternRunning { get; set; }

    void Awake()
    {
        this.SpawnInterval = 5.0f;
        this._andares = this.GameController.GetComponent<GameControllerScript>().Andares;
    }

    void Update() {
        this._score = this.GameController.GetComponent<GameControllerScript>().GameScore;
    }

    void Spawn() {
        patterns[Random.Range(0, this.patterns.Length)].GetComponent<SpawnPatterns>().StartPattern(this._andares, Veloc,this);
        PatternRunning = true;
    }

    void TrocaSpawn() {
        //codigo...
    }

    void Start() {
        StartCoroutine(SpawnCount(1));
    }

    IEnumerator SpawnCount(float mSecs) {
        yield return new WaitForSeconds(mSecs);
        if (!PatternRunning)
            Spawn();
        StartCoroutine(SpawnCount(SpawnInterval));
    }

    public void SpeedUp()
    {
        this.Veloc = this.Veloc + (this.Veloc / 10);
		print (Veloc);
		SpeedUpAnim.Play ();
        if(SpawnInterval - 1.0f >= 1.0f)
            this.SpawnInterval -= 1.0f;
    }
}
