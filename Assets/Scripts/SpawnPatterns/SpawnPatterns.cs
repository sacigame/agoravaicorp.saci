﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Assets.Scripts.Editable;

namespace Assets.Scripts.SpawnPatterns {
    public class SpawnPatterns : MonoBehaviour {
        [SerializeField]
        List<LaneSet> LaneSettings;

        [SerializeField]
        private bool Randomizable;

        private SpawnController spc;
        private int objsCounter = 0;

        public void StartPattern(List<GameObject> andares, float velocity, SpawnController spc)
        {
            this.spc = spc;
            bool isSpawnReversed = Random.Range(0, 2) > 0;
            if (Randomizable) 
				ListUtility.ShuffleItems<LaneSet>(LaneSettings);

            var objectInstances = this.LaneSettings.Select((item, index) => {
                GameObject obj = Instantiate(item.ObjectLane, new Vector2(isSpawnReversed ? item.XPosition * -1 : item.XPosition, andares[index].transform.position.y), Quaternion.identity) as GameObject;
                obj.GetComponent<Rigidbody2D>().velocity = new Vector2(obj.transform.position.x <= 0 ? velocity : -velocity, 0);
                obj.transform.Rotate(Vector3.up, (obj.transform.position.x <= 0) ? 180 : 0, Space.World);
                obj.GetComponent<AbstractSpawnableItem>().getSpawnPattern(this);
                objsCounter++;
                return obj;
            }).ToList();
        }

        public void  PatternIsFinished()
        {
            objsCounter--;
            spc.PatternRunning = objsCounter > 0;
        } 
    }
}
