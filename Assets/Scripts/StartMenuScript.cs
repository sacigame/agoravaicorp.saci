﻿using UnityEngine;
//using GooglePlayGames;
//using GooglePlayGames.BasicApi;

public class StartMenuScript : MonoBehaviour {
    [SerializeField]
    private AdsScript AdsController;

    [SerializeField]
    private UnityEngine.UI.Text lifesText;

    public GoogleAnalyticsV3 googleAnalytics;


    void Start() {
        // Start a new session.
        FirstTime();
        lifesText.text = "LIFES: " + PlayerPrefs.GetInt("Lifes") + " x ";
        googleAnalytics.StartSession();
        googleAnalytics.LogScreen("TelaInicial");

        //PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder().Build();

        //PlayGamesPlatform.InitializeInstance(config);
        //// recommended for debugging:
        //PlayGamesPlatform.DebugLogEnabled = true;
        //// Activate the Google Play Games platform
        //PlayGamesPlatform.Activate();
    }

    private void FirstTime() {
        if (PlayerPrefs.GetInt("Lifes") == 0)
            PlayerPrefs.SetInt("Lifes", 3);
    }

    public void LoadMainGameScene() {
        googleAnalytics.LogScreen("Jogo");
        CountLifesOnPlay();
        googleAnalytics.DispatchHits();
        UnityEngine.SceneManagement.SceneManager.LoadScene("Jogo");
    }

    public void LoadHowToScene() {
        googleAnalytics.LogScreen("HowTo");
        CountLifesOnPlay();
        googleAnalytics.DispatchHits();
        UnityEngine.SceneManagement.SceneManager.LoadScene("HowTo");
    }

    public void AddLife() {
        int mLifes = PlayerPrefs.GetInt("Lifes") + 1;
        PlayerPrefs.SetInt("Lifes", mLifes);
        lifesText.text = "LIFES: " + mLifes + " x ";
    }

    void CountLifesOnPlay() {
        googleAnalytics.LogEvent(new EventHitBuilder()
            .SetEventLabel("Vidas no inicio")
            .SetEventValue(PlayerPrefs.GetInt("Lifes")));
    }

    public void ShowLeaderboard() {
        //Social.ShowLeaderboardUI();
    }
}
