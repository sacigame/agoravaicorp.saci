using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using Assets.Scripts.Editable;
using UnityEngine;

public static class ListUtility {
	public static void ShuffleItems<T>( IList<T> list) where T : LaneSet {
        RNGCryptoServiceProvider provider = new RNGCryptoServiceProvider();
        int n = list.Count;
        while (n > 1) {
            byte[] box = new byte[1];
            do provider.GetBytes(box); while (!(box[0] < n * (Byte.MaxValue / n)));
            int k = (box[0] % n);
            n--;
            GameObject value = list[k].ObjectLane;
            list[k].ObjectLane = list[n].ObjectLane;
            list[n].ObjectLane = value;
        }
    }
}