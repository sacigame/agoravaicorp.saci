﻿using UnityEngine;
using System.Collections;

public class teste : MonoBehaviour {

	[SerializeField]
	GameObject[] andares;

	[SerializeField]
	GameObject prefab;

	GameObject obj;

	void Start(){
		obj = (GameObject)Instantiate (prefab, Vector3.zero, Quaternion.identity);
	}

	float x = 1;

	void Update(){
		if(Input.GetKey(KeyCode.UpArrow)){
			obj.transform.SetParent (andares [1].transform);
			obj.transform.localPosition = new Vector2 (x, 0);
		}else if(Input.GetKey(KeyCode.DownArrow)){
			obj.transform.SetParent (andares [0].transform);
			obj.transform.localPosition = new Vector2 (x, 0);
		}
	}
}
